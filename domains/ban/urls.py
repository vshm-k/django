from django.urls import path
from .views import update_banned, update_rkn_ban


urlpatterns = [
    path('', update_banned, name='banned_url'),
    path('update', update_rkn_ban, name='update_rknban_url'),
]