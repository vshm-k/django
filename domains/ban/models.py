from django.db import models
# Create your models here.


class BannedIPAddress(models.Model):
    ip = models.fields.GenericIPAddressField(unique=True)


class BannedDomain(models.Model):
    fqdn = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.fqdn


class BannedNetwork(models.Model):
    cidr = models.CharField(max_length=50, unique=True)
