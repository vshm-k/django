from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q
from .models import BannedIPAddress, BannedDomain, BannedNetwork
from .rkn_ban import update_data
import json
import os

from gandi.models import Domain
from qrator.models import IPAddress

from ipaddress import IPv4Network, IPv4Address

# Create your views here.

def get_banned_json():
    parent_dir = os.path.abspath(os.path.join(__file__, os.pardir))
    file_path = os.path.join(parent_dir, 'rkn_ban', 'data.json')
    with open(file_path) as f:
        banned = json.load(f)
    return banned

def update_banned(request):
    b = BannedDomain.objects.all().values_list('fqdn', flat=True)
    d = Domain.objects.all().values_list('fqdn', flat=True)
    x = d.intersection(b)
    Domain.objects.filter(fqdn__in=x).update(banned=True)
    Domain.objects.filter(~Q(fqdn__in=x)).update(banned=False)
    print(request.META.get('HTTP_REFERER'))
    
    return render(request, 'ban/banned_domains.html', context={'domains': x})

def update_rkn_ban(request):
    update_data()
    BannedDomain.objects.all().delete()
    BannedIPAddress.objects.all().delete()
    BannedNetwork.objects.all().delete()
    banned = get_banned_json()
    create = []

    for domain in banned['domains']:
        create.append(BannedDomain(fqdn=domain))
    BannedDomain.objects.bulk_create(create)
    create = []

    for ip in banned['ip']:
        create.append(BannedIPAddress(ip=ip))
    BannedIPAddress.objects.bulk_create(create)
    create = []

    for net in banned['networks']:
        create.append(BannedNetwork(cidr=net))
    BannedNetwork.objects.bulk_create(create)

    ban_domains = BannedDomain.objects.all().values_list('fqdn', flat=True)
    domains = Domain.objects.all().values_list('fqdn', flat=True)
    our_ban_domains = domains.intersection(ban_domains)

    Domain.objects.filter(fqdn__in=our_ban_domains).update(banned=True)
    Domain.objects.filter(~Q(fqdn__in=our_ban_domains)).update(banned=False)



    ban_ip = BannedIPAddress.objects.all().values_list('ip', flat=True)
    ip = IPAddress.objects.all().values_list('value', flat=True)
    our_ban_ip = ip.intersection(ban_ip)

    IPAddress.objects.filter(value__in=our_ban_ip).update(banned=True)
    IPAddress.objects.filter(~Q(value__in=our_ban_ip)).update(banned=False)


    banned_networks = BannedNetwork.objects.all()
    not_banned_ip = IPAddress.objects.filter(banned=False)
    for ip in not_banned_ip:
        ip.banned = False
        for net in banned_networks:
            if IPv4Address(ip.value) in IPv4Network(net.cidr):
                ip.banned = True
        ip.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
