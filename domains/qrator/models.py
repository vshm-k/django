from django.db import models
from django.shortcuts import reverse
#from gandi.models import *

# Create your models here.


class IPAddress(models.Model):
    value = models.GenericIPAddressField(unique=True)
    banned = models.BooleanField(default=True)
    qrator_pool = models.ForeignKey(
        'QratorService', on_delete=models.DO_NOTHING, null=True)
    domain = models.ForeignKey(
        'gandi.Domain', on_delete=models.DO_NOTHING, null=True)


class QratorService(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

    def get_absolute_url(self):
        return reverse('service_info_url', kwargs={'id': self.id})

    def __str__(self):
        return self.name
