from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import IPAddress, QratorService
from .api import Qrator

from ipaddress import IPv4Network, IPv4Address
# Create your views here.


def ip_list(request):
    ip = IPAddress.objects.all()
    return render(request, 'qrator/ip_list.html', context={'ip': ip})


def update_qrator_services(request):
    client = Qrator('678d8c29cff89a380456cec2f09d0ff4')
    services = client.domains_get('cl2843')
    for service in services:
        defaults = {'name': service['name'], 'status': service['status']}
        q, created = QratorService.objects.get_or_create(
            id=service['id'], defaults=defaults)
        IPAddress.objects.filter(value__in=service['ip']).update(qrator_pool=q)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def service_info(request, id):
    service = QratorService.objects.get(id=id)
    ip = IPAddress.objects.filter(qrator_pool=service)
    return render(request, 'qrator/service_info.html', context={'service': service, 'ip': ip})
