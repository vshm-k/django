import json
import requests
import socket


class Qrator:
    def __init__(self, api_key):
        self.headers = {'Content-Type': 'application/json',
                        'X-Qrator-Auth': api_key}

    def __qrator_request__(self, url, data):
        response = requests.post(
            url, headers=self.headers, data=json.dumps(data))
        result = json.loads(response.content.decode())
        if not result['error']:
            return result['result']
        else:
            return result['error']

    def domains_get(self, client_id):
        url = 'https://api.qrator.net/request/client/{}'.format(client_id)
        data = {'method': 'domains_get', 'params': 'null', 'id': 1}
        return self.__qrator_request__(url, data)

    def ping(self):
        url = 'https://api.qrator.net/request/debug/'
        data = {'method': 'ping', 'params': 'null', 'id': 1}
        return self.__qrator_request__(url, data)

    def test_iplist(self, *args):
        url = 'https://api.qrator.net/request/debug/'
        data = {'method': 'test_iplist', 'params': args, 'id': 1}
        return self.__qrator_request__(url, data)

    def test_timediplist(self, *args):
        url = 'https://api.qrator.net/request/debug/'
        data = {'method': 'test_timediplist', 'params': args, 'id': 1}
        return self.__qrator_request__(url, data)

    def service_ip_get(self, service_id):
        url = 'https://api.qrator.net/request/service/{}'.format(service_id)
        data = {'method': 'service_ip_get', 'params': 'null', 'id': 1}
        return self.__qrator_request__(url, data)

    def sorted_ip_get(self, client_id):
        domains = self.domains_get(client_id)
        ip = [item for sublist in domains for item in sublist['ip']]
        sorted_ip = sorted(ip, key=lambda item: socket.inet_aton(item))
        return list(sorted_ip)