# Generated by Django 2.2 on 2019-08-23 08:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qrator', '0002_qratorservice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipaddress',
            name='qrator_pool',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
