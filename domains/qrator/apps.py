from django.apps import AppConfig


class QratorConfig(AppConfig):
    name = 'qrator'
