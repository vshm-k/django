from django.urls import path
from .views import ip_list, update_qrator_services, service_info


urlpatterns = [
    path('', ip_list, name='ip_list_url'),
    path('update/', update_qrator_services, name='update_qrator_url'),
    path('service/<int:id>', service_info, name='service_info_url'),
]