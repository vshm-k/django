# Generated by Django 2.2 on 2019-08-22 10:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gandi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='banned',
            field=models.BooleanField(default=False),
        ),
    ]
