from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404, get_list_or_404
from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
from .api import Gandi
from .models import Domain, DnsZone, DnsRecord
from .utils import ObjectList
from .forms import DomainForm


class DomainList(ObjectList, View):
    model = Domain
    template = 'gandi/index.html'


class DNSZoneList(ObjectList, View):
    model = DnsZone
    template = 'gandi/dns_zones.html'      

class DomainDetail(View):
    def get(self, request, fqdn):
        domain = get_object_or_404(Domain, fqdn=fqdn)
        #domain.update()
        return render(request, 'gandi/domain_info.html', context={'domain': domain})

class DomainCreator(View):
    def get(self, request):
        form = DomainForm()
        return render(request, 'gandi/domain_create.html', context={'form': form})

    def post(self, request):
        bound_form = DomainForm(request.POST)
        if bound_form.is_valid():
            new_domain = bound_form.save()
            return redirect(new_domain)
        else:
            print(bound_form.errors)
        return render(request, 'gandi/domain_create.html', context={'form': bound_form})

class DNSZoneDetail(View):
    def get(self, request, id):
        zone = get_object_or_404(DnsZone, id=id)
        records = DnsRecord.objects.filter(dns_zone=zone)
        return render(request, 'gandi/dns_zone_info.html', context={'zone': zone, 'records': records})


def update_domains(request):
    client = Gandi('kG9gF648aYunL3BUxrwF2GRe')
    domains = client.domain_list()
    domains = [x['fqdn'] for x in domains]
    for name in domains:
        Domain.objects.update_or_create(
            fqdn=name
        )
    domains = Domain.objects.all()
    return redirect('gandi_domains_list_url')


def update_dns_zones(request):
    client = Gandi('kG9gF648aYunL3BUxrwF2GRe')
    zones = client.zone_list()
    for zone in zones:
        defaults = {
            'updated': zone['date_updated'],
            'version': zone['version'],
            'public': zone['public'],
            'name': zone['name']
        }
        DnsZone.objects.get_or_create(id=zone['id'], defaults=defaults)

    return redirect('dns_zones_url')


def update_zone_info(request, id):
    zone = DnsZone.objects.get(id=id)
    zone.update()
    return redirect('dns_zone_url')


def fullupdate_domains(request):
    domains = Domain.objects.all()
    for d in domains:
        d.update()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def fullupdate_dns_zones(request):
    zones = DnsZone.objects.all()
    for z in zones:
        z.update()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))