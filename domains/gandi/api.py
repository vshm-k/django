import requests
import time
import xmlrpc.client


class Gandi:
    def __init__(self, api_key):
        self.api_key = api_key
        self.client = xmlrpc.client.ServerProxy(
            'https://rpc.gandi.net/xmlrpc/', use_builtin_types=True)

    def api_version(self):
        return self.client.version.info(self.api_key)['api_version']

    def domain_count(self):
        return self.client.domain.count(self.api_key)

    def domain_list(self):
        result = list()
        page = 0
        response = self.client.domain.list(
            self.api_key,  {'items_per_page': 100, 'page': page})
        result.extend(response)
        while response:
            page += 1
            response = self.client.domain.list(
                self.api_key,  {'items_per_page': 100, 'page': page})
            result.extend(response)
        return result

    def domain_available(self, *args):
        response = self.client.domain.available(self.api_key, args)
        while'pending' in response.values():
            time.sleep(1)
            response = self.client.domain.available(self.api_key, args)
        return response

    def domain_info(self, domain):
        return self.client.domain.info(self.api_key, domain)

    def domain_host_info(self, domain):
        return self.client.domain.host.info(self.api_key, domain)

    def domain_expire_date(self, domain):
        info = self.domain_info(domain).get('date_registry_end', None)
        if info:
            return info.date().isoformat()
        else:
            return None

    def domain_autorenew_activate(self, domain):
        return self.client.domain.autorenew.activate(self.api_key, domain)

    def domain_autorenew_deactivate(self, domain):
        return self.client.domain.autorenew.deactivate(self.api_key, domain)

    def zone_list(self):
        result = list()
        page = 0
        response = self.client.domain.zone.list(
            self.api_key,  {'items_per_page': 100, 'page': page})
        result.extend(response)
        while response:
            page += 1
            response = self.client.domain.zone.list(
                self.api_key,  {'items_per_page': 100, 'page': page})
            result.extend(response)
        return result

    def zone_record_list(self, zone, version=0):
        return self.client.domain.zone.record.list(self.api_key, zone, version)
