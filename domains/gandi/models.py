from django.db import models
from django.shortcuts import reverse
from .api import Gandi
from qrator.models import IPAddress

# Create your models here.


class Domain(models.Model):
    fqdn = models.CharField(max_length=50, unique=True)
    creation_date = models.DateField(null=True)
    expire_date = models.DateField(null=True)
    autorenew = models.BooleanField(default=False)
    banned = models.BooleanField(default=False)
    dns_zone = models.ForeignKey(
        'DnsZone', on_delete=models.DO_NOTHING, blank=True, null=True)

    def update(self):
        client = Gandi('kG9gF648aYunL3BUxrwF2GRe')
        info = client.domain_info(self.fqdn)
        self.creation_date = info['date_created']
        self.expire_date = info['date_registry_end']
        autorenew = info.get('autorenew', None)
        if autorenew:
            self.autorenew = autorenew['active']
        else:
            self.autorenew = False
        zone = DnsZone.objects.filter(id=info['zone_id'])
        if zone:
            self.dns_zone = zone[0]
        self.save()

    def get_absolute_url(self):
        return reverse('domain_info_url', kwargs={'fqdn': self.fqdn})

    def __str__(self):
        return self.fqdn


class DnsZone(models.Model):
    id = models.IntegerField(primary_key=True)
    updated = models.DateField(blank=True, null=True)
    version = models.IntegerField(blank=True, null=True)
    public = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=50)

    def update(self):
        client = Gandi('kG9gF648aYunL3BUxrwF2GRe')
        info = client.zone_record_list(self.id, self.version)
        for record in info:
            defaults = {
                'name': record['name'],
                'type': record['type'],
                'value': record['value'],
                'ttl': record['ttl'],
                'dns_zone': self
            }
            DnsRecord.objects.get_or_create(id=record['id'], defaults=defaults)

            ipaddress = IPAddress.objects.filter(value=record['value'])
            if ipaddress:
                ip = ipaddress[0]
                domain = Domain.objects.filter(dns_zone=self)
                if domain:
                    ip.domain=domain[0]
                    ip.save()


        return reverse('dns_zone_url', kwargs={'id': self.id})

    def get_absolute_url(self):
        return reverse('dns_zone_url', kwargs={'id': self.id})

    def __str__(self):
        return self.name


class DnsRecord(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=10)
    value = models.GenericIPAddressField()
    ttl = models.IntegerField()
    dns_zone = models.ForeignKey('DnsZone', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
