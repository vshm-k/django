from django.urls import path
from .views import *


urlpatterns = [
    path('', DomainList.as_view(), name='gandi_domains_list_url'),
    path('dns_zones/', DNSZoneList.as_view(), name='dns_zones_url'),
    path('domain/create', DomainCreator.as_view(), name='domain_create'),
    path('domain/<str:fqdn>', DomainDetail.as_view(), name='domain_info_url'),
    path('domains/update', update_domains, name='update_domains_url'),
    path('domains/fullupdate', fullupdate_domains, name='fullupdate_domains_url'),
    path('dns_zone/update', update_zone_info, name='update_dns_zone_url'),
    path('dns_zone/<int:id>', DNSZoneDetail.as_view(), name='dns_zone_url'),
    path('dns_zones/update', update_dns_zones, name='update_dns_zones_url'),
    path('dns_zones/fullupdate', fullupdate_dns_zones, name='fullupdate_dns_zones_url'),
]