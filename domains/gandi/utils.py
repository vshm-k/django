from django.shortcuts import render
from django.shortcuts import get_list_or_404





class ObjectList:
    model = None
    template = None

    def get(self, request):
        objects = get_list_or_404(self.model)
        context_name = self.model.__name__.lower() + '_list'
        return render(request, self.template, context={context_name: objects})