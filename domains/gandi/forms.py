from django import forms
from .models import Domain
import datetime
from django.core.exceptions import ValidationError


class DomainForm(forms.ModelForm):
    # fqdn = forms.CharField(max_length=50)
    # creation_date = forms.DateField(initial=datetime.date.today)
    # expire_date = forms.DateField(initial=(datetime.date.today() + datetime.timedelta(days=365)))

    # fqdn.widget.attrs.update({'class': 'form-control'})
    # creation_date.widget.attrs.update({'class': 'form-control'})
    # expire_date.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Domain
        fields = ['fqdn', 'creation_date', 'expire_date']

        widgets = {
            'fqdn': forms.TextInput(attrs={'class': 'form-control'}),
            'creation_date': forms.DateInput(attrs={'class': 'form-control', 'value': datetime.date.today()}),
            'expire_date': forms.DateInput(attrs={'class': 'form-control', 'value':datetime.date.today() + datetime.timedelta(days=365)})
        }

    def clean_fqdn(self):
        fqdn = self.cleaned_data['fqdn']
        domain = Domain.objects.filter(fqdn=fqdn)
        if domain:
            raise ValidationError('fqdn must be unique. Domain "{}" already exist'.format(fqdn))
        return fqdn


    # def save(self):
    #     domain = Domain(fqdn=self.cleaned_data['fqdn'],
    #                     creation_date=self.cleaned_data['creation_date'],
    #                     expire_date=self.cleaned_data['expire_date'],
    #                     autorenew=False,
    #                     banned=False,
    #                     dns_zone=None)

    #     domain.save()
    #     return domain
