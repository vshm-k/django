from django.apps import AppConfig


class GandiConfig(AppConfig):
    name = 'gandi'
