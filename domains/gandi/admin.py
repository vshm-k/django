from django.contrib import admin

# Register your models here.

from .models import Domain, DnsZone, DnsRecord

admin.site.register(Domain)
admin.site.register(DnsZone)
admin.site.register(DnsRecord)